//Load in these packages
var app = require('express')(); //express package
var http = require('http').Server(app); //http server package from express
var io = require('socket.io')(http);

//When a get request occurs at root of site
//it sends an html file to be displayed
app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html'); //note res.sendFile
});

io.on('connection', function(socket){
  socket.on('chat message', function(msg){
    //console.log('message: ' + msg);
    io.emit('chat message', msg);
  });
});

//Always listen on this port and notify the console.
http.listen(3000, function(){
  console.log('listening on *:3000');
});
